#include "Listener.h"

std::string Listener::stripQuotes(std::string s){
    if(s.empty()||s.at(0)!='"') return s;
    return s.substr(1, s.size()-2);
}

void Listener::exitJsonObject(JSONParser::JsonObjectContext *ctx){
    setXML(ctx, getXML(ctx->object()));  // 这里ctx是在JSONParser里声明了的, 如果不清楚里面的东西可以看一下相应的文件
}

void Listener::exitJsonArray(JSONParser::JsonArrayContext *ctx){
    setXML(ctx, getXML(ctx->array()));  // 可以通过这种方式获得其在产生式中的右部
}

void Listener::exitAnObject(JSONParser::AnObjectContext *ctx){
    std::string s="\n";
    for(auto pctx: ctx->pair()){
        s += getXML(pctx);
    }
    setXML(ctx, s);
}

void Listener::exitEmptyObject(JSONParser::EmptyObjectContext *ctx){
    setXML(ctx, std::string(""));
}

void Listener::exitArrayOfValues(JSONParser::ArrayOfValuesContext *ctx){
    std::string s = "\n";
    for(auto vctx: ctx->value()){
        s += "<element>";
        s += getXML(vctx);
        s += "</element>";
        s += "\n";
    }
    setXML(ctx, s);
}

void Listener::exitEmptyArray(JSONParser::EmptyArrayContext *ctx){
    setXML(ctx, std::string(""));
}

void Listener::exitPair(JSONParser::PairContext *ctx){
    std::string tag = stripQuotes(ctx->STRING()->getText());
    auto vctx = ctx->value();
    std::string s = "<"+tag+">"+getXML(vctx)+"</"+tag+">\n";
    setXML(ctx, s);
}

void Listener::exitObjectValue(JSONParser::ObjectValueContext *ctx){
    setXML(ctx, getXML(ctx->object()));
}

void Listener::exitArrayValue(JSONParser::ArrayValueContext *ctx){
    setXML(ctx, getXML(ctx->array()));
}

void Listener::exitAtom(JSONParser::AtomContext *ctx){
    setXML(ctx, ctx->getText());
}

void Listener::exitString(JSONParser::StringContext *ctx){
    setXML(ctx, stripQuotes(ctx->getText()));
}