#include <string>

#include "JSONBaseListener.h"

class Listener: public JSONBaseListener{
    public:
        // 这部分是对在JSON.g4中相应产生式的语义动作函数, 继承自父类
        void exitJsonObject(JSONParser::JsonObjectContext *ctx) override;
        void exitJsonArray(JSONParser::JsonArrayContext *ctx) override;
        void exitAnObject(JSONParser::AnObjectContext *ctx) override;
        void exitEmptyObject(JSONParser::EmptyObjectContext *ctx) override;
        void exitArrayOfValues(JSONParser::ArrayOfValuesContext *ctx) override;
        void exitEmptyArray(JSONParser::EmptyArrayContext *ctx) override;
        void exitPair(JSONParser::PairContext *ctx) override;
        void exitObjectValue(JSONParser::ObjectValueContext *ctx) override;
        void exitArrayValue(JSONParser::ArrayValueContext *ctx) override;
        void exitAtom(JSONParser::AtomContext *ctx) override;
        void exitString(JSONParser::StringContext *ctx) override;
        // 这两个是辅助函数, 用来方便放入和取出注释分析树
        std::string getXML(antlr4::tree::ParseTree *node) {return xml.get(node);};
        void setXML(antlr4::tree::ParseTree *node, std::string str) {xml.put(node, str);};
    private:
        // ParseTreeProperty是antlr4封装的一个map, 可以理解为注释分析树
        antlr4::tree::ParseTreeProperty<std::string> xml;
        // 辅助函数, 用来处理字符串
        std::string stripQuotes(std::string s);
};
