#include <iostream>
#include <string>

#include "antlr4-runtime.h"

#include "JSONLexer.h"
#include "JSONParser.h"
#include "Listener.h"

int main(int argc, const char *argv[]){
    if (argc != 2) {
        std::cout<<"wrong input format"<<std::endl;
        return 1;
    }

    std::ifstream stream(argv[1], std::ios::binary);
    // 将输入文件放到antlr的input stream中
    antlr4::ANTLRInputStream input(stream);
    // 词法解析
    JSONLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);
    tokens.fill();
    // 语法解析
    JSONParser parser(&tokens);
    parser.setBuildParseTree(true);
    // 从json开始解析语法树
    antlr4::tree::ParseTree* tree = parser.json();
    Listener listener;
    // 以listener遍历语法树tree
    antlr4::tree::ParseTreeWalker::DEFAULT.walk(&listener, tree);
    // 输出遍历结果
    std::cout<<listener.getXML(tree)<<std::endl;
    return 0;
}
