1. Hello.g4为antlr4的语法描述文件。antlr4采用EBNF来描述基本的词法语法。
   1. 第一行grammar Hello;标识了此语法名字，这里必须与文件名相同。
   2. 下面则是具体的语法以及词法。
   3. 最后一行的->skip则是antlr4中的通道机制，此处的意义为将匹配到的空白字符放到skip通道里，即跳过。详细的通道机制可查阅推荐教程学习。
2. antlr4是基于Java编写的，可以生成多种语言的词法语法解析器生成器；
   1. `antlr4 Hello.g4`：运行此命令即可生成相应的由Java编写的解析器代码。
   2. `javac *.java`：运行此命令进行编译
   3. `grun Hello r -tokens test.txt`：此命令表示利用antlr4提供的测试框架对语法文件进行测试；
      1. 其中Hello表示语法名，而r表示从具体哪条产生式开始处理，-tokens表示输出类型为token，test.txt则表示输入文件
      2. 可以将-tokens替换为-tree展示类似LISP语法的树结构
      3. 可以将-tokens替换为-gui展示可视化分析树（若出现错误则按照错误类型安装或更改文件即可）
3. antlr4生成的分析器代码文件说明
   1. HelloParser.java: 包含了语法解析器类型定义
   2. HelloLexer.java: 包含了词法解析器类型定义
   3. Hello.tokens:  antlr4将每个token用数表示, 此文件存储了相应值
   4. HelloListener.java: listner接口, 表示了我们可以实现那些方法
   5. HelloBaseListener.java: 一个空的listener实现, 应当重写这里的方法以实现相应的语义动作.