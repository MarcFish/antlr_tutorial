1. ANTLR4简介
   1. ANTLR是基于LL(*)算法实现的语法解析器生成器，用Java语言编写，由旧金山大学的Terence Parr博士等人于1989年开始发展。
   2. ANTLR能够生成基于listener或visitor模式的由Java, C++等语言编写的词法语法解析器。
   3. ANTLR文法定义使用EBNF的定义方式；ANTLR支持消除直接左递归；
   4. 应用于：Groovy；Jython；Twitter’s search query language等。
2. 安装: https://www.antlr.org/ 上就有安装方法;需要提前安装Java;下述命令仅安装了ANTLR4工具，运行时则需要针对不同代码选择安装。
```
$ cd /usr/local/lib
$ sudo curl -O https://www.antlr.org/download/antlr-4.9.2-complete.jar
$ export CLASSPATH=".:/usr/local/lib/antlr-4.9.2-complete.jar:$CLASSPATH"
$ alias antlr4='java -jar /usr/local/lib/antlr-4.9.2-complete.jar'
$ alias grun='java org.antlr.v4.gui.TestRig'
```
3. 教程推荐
   1. 《The Definitive ANTLR 4 Reference》
4. 简要说明: 这一部分可以结合同目录下几个例子一起看
   1. 概念:
      1. antlr4 工具: 读入一个语法描述文件并生成语法解析和词法解析
      2. antlr4运行时: 一个库, 其中包含了所生成代码运行需要的类和方法.
      3. listeners模式: 无需手动遍历语法树, 只需要实现listener方法.
      4. visitors模式: 需要手动遍历语法树, 语法树的遍历是手动控制的. 在生成时需要添加命令`-no-listener -visitor`
   2. 优先级, 左递归以及结合性
      1. 优先级: antlr4中隐式设定先出现的优先级高
      2. 左递归
         1. antlr4能够处理直接左递归, 不需要对产生式做改变
         2. 不能处理间接左递归
      3. 结合性
         1. 在产生式中插入`<assoc=right>`
   3. 如何在遍历树的过程中共享信息;antlr4自动生成所有的listener方法, 并没有为特定的引用返回值或参数等.
      1. 使用visitors遍历解析树: antlr4生成的visitor是方法中通过模板的方法, 允许自定义返回值类型.
      2. 采用基于栈的方式来存储返回值
      3. 基于map的标注树: 将解析树节点作为key而值作为value;
         1. 自定义map
         2. 采用antlr4提供的ParseTree类
5. 此仓库提供了ANTLR4的简要教程，采用的是C++完成的。
   1. 环境为Java8或更高版本；ANTLR4：4.9.2
6. 建议
   1. antlr4的github中包含了很多例子, 可以参考.
   2. antlr4中github中包含了很多已有语言的语法产生式, 可以直接使用或参考.
